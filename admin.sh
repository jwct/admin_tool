#!/bin/bash
function main_prompt() {
    clear
    echo ""
    echo "0) top"
    echo "1) vmstat 5 20"
    echo "2) list firewall"
    echo "3) fail2ban-client status"
    echo "e1) firewall rule"
    echo "q) quit "
    echo ""
}

function presskey(){
    read -p "press ENTER to continue"
}

function firewall_task() {
    echo ""
    echo "1) add    443,30022"
    echo "2) remove 443,30022" 
    echo "3) temporary add 80"
    echo "r) reload "
    echo "" 
    read 
    case $REPLY in
        1)
           firewall-cmd --add-port=443/tcp
           firewall-cmd --add-port=30022/tcp
           firewall-cmd --list-all
           presskey ;;
        2) 
           firewall-cmd --remove-port=443/tcp
           firewall-cmd --remove-port=30022/tcp
           firewall-cmd --list-all
           presskey ;;
        3)
           firewall-cmd --add-port=80/tcp
           presskey ;;
        r) 
           firewall-cmd --reload
           presskey ;;
        *) echo "What?";;
    esac
}

function main_task(){
    main_prompt
    while read && [[ $REPLY != q ]]; do
        case  $REPLY in 
         0) top ;;
         1) vmstat 5 20;;
         2) firewall-cmd --list-all 
            presskey ;;
         3) fail2ban-client status 
            presskey ;;
         e1) firewall_task;;
         *) echo "What?";;
        esac
    main_prompt
    done
}


main_task

