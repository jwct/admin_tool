#!/bin/bash

########
# step 1 : get free cert
#certbot certonly

########
# step 2 : 
DEV_USER=dev_user
DOMAIN=my.domain
SRC_PATH=/etc/letsencrypt/live/$DOMAIN
DEST_PATH=/home/$DEV_USER/repo/reverse_proxy/src/cert

SRC=privkey.pem
DEST=private.key
cp $SRC_PATH/$SRC $DEST_PATH/$DEST
chown $DEV_USER $DEST_PATH/$DEST


SRC=fullchain.pem
DEST=certificate.crt
cp $SRC_PATH/$SRC $DEST_PATH/$DEST
chown $DEV_USER $DEST_PATH/$DEST


